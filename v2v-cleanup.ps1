﻿param([array]$vms)

clear

write-host "Clean up for failed convert jobs" -ForegroundColor Yellow
write-host
write-host "Need to connect to vCenter to rename VMs if they exist, so asking for credentials"
sleep -s 2
$cred = get-credential
$vc = $null
$vc = connect-viserver t2usvcenter01.t2hosted.com -Credential $cred
If($vc)
{
    $list = $null
    If(!($vms))
    {
write-host @"
You can type a path to a txt file, or
type a comma-seperated list, or
type just one VM.
"@
        write-host
        $vms = read-host "VM(s)"
    }
    If($vms -match ":")
    {
        If(Test-Path $vms.trim()){$list = gc $vms.trim()}
        Else{write-host "Seems to be a problem with the path to the txt file!" -ForegroundColor Red}
    }
    Else{$list = ($vms.trim()).split(",")}

    If($list)
    {
        write-host "Most VMs have their Windows Firewall turned on, but some don't."
        write-host "Please look in the csv file that was created during the v2v-pre-config"
        write-host "script to see what each VM's firewall was at the time of the run of"
        write-host "the script.  You will be asked if the firewall was On or Off for each VM."
        write-host
        write-host "WINDOWS FIREWALL" -ForegroundColor Cyan

        $cleanit = @()
        $list | %{
            $c = "" | select VM,WF,Issue
            $c.vm = $_
            $c.wf = read-host "$($_) - On or Off"
            $cleanit += $c
        }
        $cleanit | %{
            $vm = $null
            $vm = get-vm $_.vm -ErrorAction SilentlyContinue
            If($vm -and $vm.PowerState -eq 'PoweredOff'){$vm | Set-VM -Name "$($_.vm)_delete" -Confirm:$false}
            If(test-path "\\$($_.vm)\c$\IT\info_v2v.txt"){Remove-Item "\\$($_.vm)\c$\IT\info_v2v.txt"}
            If(test-path "\\$($_.vm)\c$\IT\devcon.exe"){Remove-Item "\\$($_.vm)\c$\IT\devcon.exe"}
            If(test-path "\\$($_.vm)\c$\IT\v2v-post-config.ps1"){Remove-Item "\\$($_.vm)\c$\IT\v2v-post-config.ps1"}
            If(get-scheduledtask -taskname V2v-Post -cimsession $_.vm){Unregister-ScheduledTask -TaskName "V2V-Post" -CimSession $_.vm -Confirm:$false}
            If($_.wf -match "On"){Set-NetFirewallProfile -CimSession $_.vm -Profile Domain -Enabled True}

        }

    }
    disconnect-viserver -Confirm:$false
}
Else{write-host "Trouble connecting to vCenter!" -ForegroundColor red}