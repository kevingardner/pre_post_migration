﻿param([Parameter(Mandatory=$true)]
$hyperv)

function ch-mod
{
param($hypv)
$hypmod = $null
$hypmod = get-module -name hyper-v
$hypos = gwmi win32_operatingsystem -ComputerName $hypv
If($hypos.caption -match "2012" -and ($hypmod.version.major -ne 1 -or !($hypmod)))
{
    If($hypmod){Remove-Module -Name hyper-v}
    import-module -Name hyper-v -RequiredVersion 1.1 -Prefix HYP -Scope Global
}
ElseIf($hypos.caption -match "2016|2019" -and ($hypmod.version.major -ge 2 -or !($hypmod)))
{
    If($hypmod){Remove-Module -Name hyper-v}
    import-module -Name hyper-v -Prefix HYP -Scope Global
}
}

$list = $null
If($hyperv -match ":")
{
    If(test-path $hyperv){$list = gc $hyperv}
    Else{write-host "Cannot find list - $($hyperv)" -ForegroundColor Red}
}
Else{$list = $hyperv}
If($list)
{
    $res = @()
    $ping = New-Object system.net.networkinformation.ping
    $auser = "$($_.name)\administrator"
    $passwd = ConvertTo-SecureString "G1v3M3L0c@l76!" -AsPlainText -Force
    $cred = New-Object System.Management.Automation.PSCredential ($auser,$passwd)
    $list | %{
        $hypv = $_
        write-host $hypv
        ch-mod -hypv $hypv
        get-hypvm -computername $hypv | ?{$_.state -eq 'Running'} | %{
            $prob = $null
            $r = "" | select VM,OS,Admin,NICs,PrimaryIPInfo,AdditionalIPs,Ping,Drives,CDROM,Firewall,Hyper-V,Issue
            $vm = $_.name
            write-host "`t$($vm)"
            $r.vm = $vm
            $fos = $Null
            $fos = gwmi win32_operatingsystem -ComputerName $vm -ErrorAction SilentlyContinue
            If($fos -and $fos.caption -match 'windows')
            {
                $r.os = $fos.caption
                $afos = $null
                try{$afos = gwmi win32_operatingsystem -ComputerName $vm -Credential $cred -ErrorAction SilentlyContinue}
                catch{$r.admin = "Bad"}
                If($afos){$r.admin = "Good"}
                $r.nics = ($_.networkadapters | measure-object).count
                try{$fnet = Get-NetIPConfiguration -CimSession $vm -ErrorAction SilentlyContinue}
                catch{$prob += "Not able to get NIC, "}
                If($fnet)
                {
                    $ips = $null
                    try{$ips = get-netipaddress -CimSession $vm -AddressFamily IPv4 -ErrorAction SilentlyContinue | ?{$_.InterfaceAlias -notmatch "Loopback"}}
                    catch{$prob += "Not able to get IPs, "}
                    If($ips)
                    {
                        $prip = $ips | ?{$_.SkipAsSource -eq $false}
                        $r.primaryipinfo = "$($prip.IPAddress)|$($prip.prefixlength)|$($fnet.IPv4DefaultGateway.nexthop)"
                        $faip = $null
                        $faip = $ips.IPAddress | ?{$_ -ne $prip.IPAddress}
                        If($faip)
                        {
                            $i = $null
                            $faip | %{
                                $i += "$($_),"
                            }
                            $l = $i.Length
                            $r.additionalips = $i.Substring(0,$l-1)
                        }
                        Else{$r.additionalips = "None"}
                    }
                }
                $chping = $null
                $chping = $ping.send($vm).status
                If($chping -eq $Null){$r.ping = "Problem Pinging"}
                ElseIf($chping -ne "Success"){$r.ping = "No Ping"}
                Else{$r.ping = $chping}
                $logdr = $null
                gwmi win32_logicaldisk -Filter "DriveType = '3'" -ComputerName $vm -ErrorAction SilentlyContinue | select PSComputername,DeviceID,DriveType,Size,VolumeSerialNumber | %{
                    $logdr += "$($_.deviceid)|$($_.drivetype)|$([math]::round($_.size/1gb,0))|$($_.volumeserialnumber),"
                }
                If($logdr)
                {
                    $l = $logdr.Length
                    $r.drives = $logdr.Substring(0,$l-1)
                }
                Else{$prob += "Not able to get Drive info, "}
                $cdrom = $null
                $cdrom = gwmi win32_volume -filter "DriveType = '5'" -ComputerName $vm -ErrorAction SilentlyContinue
                If($cdrom){$r.cdrom = $cdrom.driveletter}
                Else{$r.cdrom = "None"}
                try{$wfw = Get-NetFirewallProfile -CimSession $vm -Profile Domain -ErrorAction SilentlyContinue}
                catch{$prob += "Not able to get firewall, "}
                If($wfw.Enabled -eq $true){$r.firewall = "On"}
                ElseIf($wfw.Enabled -eq $false){$r.firewall = "Off"}
            }
            Else{$prob += "Not Windows or Connection issue, "}
            $r."hyper-v" = $hypv
            If($prob)
            {
                $l = $prob.Length
                $r.issue = $prob.Substring(0,$l-2)
            }
            $res += $r
        }
    }
    If(($res | measure-object).count -ge 1)
    {
        write-host "Do you want to display the results?"
        $ans = read-host "Display"
        If($ans -match 'y'){$res}
        write-host
        write-host "Do you want to save the results to a csv file?"
        $ans = read-host "Save"
        If($ans -match 'y')
        {
            $rpath = read-host "Please provide a path and filename"
            $res | export-csv $rpath -NoTypeInformation
        }
    }
}