#*******************************************************************************
# V2V migration pre script.  Happens before running the VMware vCenter Converter
# tool.  Can be ran with or without parameters.  The -vms can be a path to a
# txt file, or comma-seperated list of VMs.  The -to can be comma-sperated also.
# If ran without parameters, it will ask for the information.
# Below are examples:
# .\v2v-pre-config.ps1 -vms C:\IT\hyperlist.txt -to kevin.gardner@t2systems.com
# .\v2v-pre-config.ps1 -vms Server1,Server2
# .\v2v-pre-config.ps1 -to kevin.gardner@t2systems.com,mitchbaker@t2systems.com
#*******************************************************************************
param([array]$vms,
$to)

clear
$ErrorActionPreference = 'silentlycontinue'
$ping = New-Object system.net.networkinformation.ping
$spath = "C:\IT"
$passwd = ConvertTo-SecureString "G1v3M3L0c@l76!" -AsPlainText -Force

write-host "V2V migration help from Hyper-V to VMware" -ForegroundColor Green
write-host
#**************************************************
# Testing if the v2v-post-config.ps1 and devcon.exe
# exist in the C:\IT\ directory. If not, then ask
# for the location.
#**************************************************
If(!(test-path $spath)){new-item -path "C:\IT" -type directory -Force}
If(!(test-path "$($spath)\v2v-post-config.ps1"))
{
    write-host "Cannot find the v2v-post-config.ps1 file!" -ForegroundColor Red
    write-host "Please provide the location of the file"
    $cpath = read-host "Path"
    write-host
}
Else{$cpath = "$($spath)\v2v-post-config.ps1"}
If(!(test-path "$($spath)\devcon.exe"))
{
    write-host "Cannot find the devcon.exe file!" -ForegroundColor Red
    write-host "Please provide the location of the file"
    $dpath = read-host "Path"
    write-host
}
Else{$dpath = "$($spath)\devcon.exe"}
If(!(test-path $cpath) -or !(test-path $dpath)){write-host "Cannot find v2v-post-config.ps1 and\or devcon.exe, so will not proceed!" -ForegroundColor Red}
Else
{    
    $list = $null
    If(!($vms))
    {
    write-host @"
You can type a path to a txt file, or
type a comma-seperated list, or
type just one VM.
"@
        write-host
        $vms = read-host "VM(s)"
    }
    If($vms -match ":")
    {
        If(Test-Path $vms.trim()){$list = gc $vms.trim()}
        Else{write-host "Seems to be a problem with the path to the txt file!" -ForegroundColor Red}
    }
    Else{$list = ($vms.trim()).split(",")}
#**************************************************
# Starts going through the list of VMs that were
# provided to check and get info for ping, drives,
# CDROM, DNS, IP, the NIC's registry entry, and if
# the firewall is on.
#**************************************************
    If($list)
    {
        $res = @()
        write-host "Gathering info for each VM." -ForegroundColor Cyan
        write-host
        $list | %{
            $vm = $_.trim()
            write-host "$($vm)..." -NoNewline
            $r = "" | select VM,Ping,Drives,CDROM,DNS,PrimaryIP,AdditionalIPs,NICReg,EMail,Issue,Firewall
            $r.vm = $vm
            $issue = $null
            $chping = $null
            $chping = $ping.send($vm).status
            If($chping -eq $Null){$r.ping = "Problem Pinging";write-host "Issue Pinging!" -ForegroundColor Yellow -NoNewline}
            ElseIf($chping -ne "Success"){$r.ping = "No Ping";write-host "No Ping!" -ForegroundColor Red -NoNewline}
            Else{$r.ping = $chping;write-host "Ping Success" -ForegroundColor Green -NoNewline}
        
            write-host "...Getting Drive and Network info..." -NoNewline
            $logdr = $null
            gwmi win32_logicaldisk -Filter "DriveType = '3'" -ComputerName $vm -ErrorAction SilentlyContinue | select PSComputername,DeviceID,DriveType,Size,VolumeSerialNumber | %{
                $logdr += "$($_.deviceid)|$($_.drivetype)|$([math]::round($_.size/1gb,0))|$($_.volumeserialnumber),"
            }
            If($logdr)
            {
                $l = $logdr.Length
                $r.drives = $logdr.Substring(0,$l-1)
            }
            Else{$issue += "Access or Drives, "}
            $cdrom = $null
            $cdrom = gwmi win32_volume -filter "DriveType = '5'" -ComputerName $vm -ErrorAction SilentlyContinue
            If($cdrom){$r.cdrom = $cdrom.driveletter}
            Else{$r.cdrom = "None"}

            $dnsinfo = $null
            $fnet = Get-NetIPConfiguration -CimSession $vm -ErrorAction SilentlyContinue
            If($fnet)
            {
                ($fnet | Select-Object -ExpandProperty DNSServer | ?{$_.addressfamily -eq 2}).serveraddresses | %{
                    $dnsinfo += "$($_),"
                }
                $l = $dnsinfo.Length
                $r.dns = $dnsinfo.Substring(0,$l-1)
                $ips = $null
                $ips = get-netipaddress -CimSession $vm -AddressFamily IPv4 -ErrorAction SilentlyContinue | ?{$_.InterfaceAlias -notmatch "Loopback"}
                If($ips)
                {
                    If(($ips | ?{$_.SkipAsSource -eq $false} | measure-object).count -eq 1)
                    {
                        $prip = $ips | ?{$_.SkipAsSource -eq $false}
                        $r.primaryip = "$($prip.IPAddress)|$($prip.prefixlength)|$($fnet.IPv4DefaultGateway.nexthop)"
                        $faip = $null
                        $faip = $ips.IPAddress | ?{$_ -ne $prip.IPAddress}
                        If($faip)
                        {
                            $i = $null
                            $faip | %{
                                $i += "$($_),"
                            }
                            $l = $i.Length
                            $r.additionalips = $i.Substring(0,$l-1)
                        }
                        Else{$r.additionalips = "None"}
                    }
                    Else{$issue += "Multiple Primary IPs, "}
                }
                Else{$issue += "Access or Network, "}
            }
            Else{$issue += "Access or Network, "}
            $nicreg = $null
            $nicreg = (get-netadapter -CimSession $vm).PnPDeviceID
            If(($nicreg | measure-object).count -eq 1){$r.nicreg = $nicreg}
            ElseIf(($nicreg | measure-object).count -gt 1){$issue += "Multiple NICs, "}
            Else{$issue += "Did not find registry key for NIC, "}

            write-host "Checking Local Admin creds..." -NoNewline
            $auser = "$($vm)\administrator"
            $cred = New-Object System.Management.Automation.PSCredential ($auser,$passwd)
            $afos = $null
            $afos = gwmi win32_operatingsystem -ComputerName $vm -Credential $cred -ErrorAction SilentlyContinue
            If(!($afos)){$issue += "Local Admin Access, "}
            If($issue)
            {
                $l = $issue.Length
                $r.issue = $issue.Substring(0,$l-2)
            }
            $wfw = Get-NetFirewallProfile -CimSession $vm -Profile Domain -ErrorAction SilentlyContinue
            If($wfw.Enabled -eq $true){$r.firewall = "On"}
            ElseIf($wfw.Enabled -eq $false){$r.firewall = "Off"}
            write-host "Done!" -ForegroundColor Cyan
            $res += $r
        }
        write-host
        $ErrorActionPreference = 'Continue'
#**************************************************
# End of gathering info for each VM, and will
# display the problem VMs and the ones that will go
# forward.  Will ask if want to continue with the
# VMs that are good.
#**************************************************
        If(($res | ?{!($_.drives -or $_.dns -or $_.primaryip) -or $_.issue} | measure-object).count -ge 1)
        {
            write-host "Problem VMs!" -ForegroundColor Yellow
            $res | ?{!($_.drives -or $_.dns -or $_.primaryip) -or $_.issue} | Select VM,Issue | ft
            write-host
        }
        If(($res | ?{$_.drives -and $_.dns -and $_.primaryip -and !($_.issue)} | measure-object).count -ge 1)
        {
            Write-host "VMs that will have their configs checked and changed after migration!" -ForegroundColor Cyan
            $runit = $res | ?{$_.drives -and $_.dns -and $_.primaryip -and !($_.issue)}
            $runit | select VM,Drives,DNS,PrimaryIP,AdditionalIPs | ft
            write-host
            write-host @"
If you continue, a scheduled task will be setup on the VM(s) to run 
after migration when VMware Tools service starts that will check and
change the drives, NIC, and IP configurations.  If the Windows firewall
for the Domain profile is truned on, it will be turned off before
migration, and turned back on after as part of the script that runs.
"@
            write-host
            write-host "Do you want to continue?" -ForegroundColor Yellow
            $ans = read-host "Y or N"
            write-host
#**************************************************
# If Y is answered to continue then the
# v2v-post-config.ps1 script and the devcon.exe file
# are copied to each VM along with the creation of
# the info txt file that has the drive info, IP
# settings, NIC registry, if the firewall is on, and
# email addresses.  It will also create a schedule
# task on each VM to kick off the v2v-post-config.ps1
# script.
#**************************************************
            If($ans -match 'y')
            {
                If($to){$to = "noc@t2systems.com,$($to)"}
                Else{$to = "noc@t2systems.com"}
                $runit | %{
                    $_.email = $to.trim()
                    $v = $_.vm
                    ($res | ?{$_.vm -eq $v}).email = $to.trim()
                    write-host "$($_.vm)...Creating info txt file..." -NoNewline
                    If(!(test-path "\\$($_.vm)\c$\IT")){new-item -path "\\$($_.vm)\c$\IT" -type directory -Force}
                    $_.vm,$_.drives,$_.cdrom,$_.dns,$_.primaryip,$_.additionalips,$_.email,$_.nicreg,$_.firewall | out-file "\\$($_.vm)\c$\IT\info_v2v.txt"
                    write-host "Copying script..." -NoNewline
                    copy-item -Path $cpath -Destination "\\$($_.vm)\c$\IT"
                    copy-item -path $dpath -Destination "\\$($_.vm)\c$\IT"
                    $auser = "$($_.vm)\administrator"
                    write-host "Creating scheduled task..." -NoNewline
                    $action = New-ScheduledTaskAction -Execute 'C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe' -argument "& C:\IT\v2v-post-config.ps1"
                    $class = cimclass MSFT_TaskEventTrigger root/Microsoft/Windows/TaskScheduler
                    $trigger = $class | New-CimInstance -ClientOnly
                    $trigger.Enabled = $true
                    $trigger.Delay = "PT1M"
                    $trigger.Subscription = '<QueryList><Query Id="0" Path="Application"><Select Path="Application">*[System[Provider[@Name=''VMTools''] and EventID=105]]</Select></Query></QueryList>'
                    $task = Register-ScheduledTask -Action $action -Trigger $trigger -TaskName V2V-Post -RunLevel Highest -User $auser -Password G1v3M3L0c@l76! -CimSession $_.vm
                    If($_.firewall -eq "On")
                    {
                        write-host "Disabling Windows Firewall for the Domain Profile..." -NoNewline
                        Set-NetFirewallProfile -CimSession $_.vm -Profile Domain -Enabled False
                    }
                    write-host "Done!"
                }
                $d = get-date
                $csvpath = "$($spath)\v2v_$($d.month)$($d.day)$($d.year)_$($d.hour)$($d.minute)_$($d.second).csv"
                $res | export-csv $csvpath -NoTypeInformation
                write-host
                write-host "Next step is VMware vCenter Converter tool.  " -ForegroundColor Cyan
                write-host "You will recieve an email for each VM in the above list " -NoNewline
                write-host "IF " -ForegroundColor Yellow -NoNewline
                write-host "the IP is able to be established."
                write-host "If you don't recieve an email, you will need to check the VM's network."
                write-host "***PLEASE BE SURE TO SELECT INSTALL VMWARE TOOLS***" -ForegroundColor Yellow -NoNewline
                write-host "  Otherwise, the script will not run"
                write-host "Also, a csv file has been saved to " -nonewline
                write-host $csvpath -ForegroundColor Green -NoNewline
                write-host " that has the info for each VM inputted."
                write-host
            }
        }
    }
}