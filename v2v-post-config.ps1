#*******************************************************************************
# V2V migration post script.  Happens after running the VMware vCenter Converter
# tool and after VMware Tools installed on the migrated VM.  This script is ran
# based on a scheduled task that looks for event ID 105 from source VMTools in
# the Windows Application log.  Can also be ran by typing .\v2v-post-config.ps1
# NOTE - VMware Tools has to be installed and running for it to do any changes
#*******************************************************************************
$fserv = $null
$fserv = get-service -DisplayName "VMware Tools"
$prob = @()

If($fserv -and $fserv.Status -eq "Running")
{
    $cominfo = gc "C:\IT\info_v2v.txt"
    $srv = $cominfo[0]
    $to = $cominfo[6].split(",")
    $dri = $cominfo[1].split(",")
    $cddl = $cominfo[2]
    $dns = $cominfo[3].split(",")
    $priIP = $cominfo[4].split("|")
    $addIP = $Null
    If($cominfo[5] -ne "None"){$addIP = $cominfo[5].split(",")}
    $nicreg = $cominfo[7]
    $wfw = $cominfo[8]

#**************************************************
# Gets the existing drive letter for the CDROM, and
# if it's not equal to what was the drive letter
# it will look to see if it's drive letter is
# available, and if it's not, then it will change
# the drive that has that drive letter to the next
# available drive letter, and then change the CDROM
# to it's original drive letter
#**************************************************
    $cdrom = gwmi win32_volume -filter "DriveType = '5'"
    $dls = @()
    for([byte]$d = [char]'E';$d -le [char]'L';$d++){$dls += "$([char]$d):"}
    for([byte]$d = [char]'N';$d -le [char]'Z';$d++){$dls += "$([char]$d):"}
    If($cddl -ne "None" -and $cdrom.driveletter -ne $cddl)
    {
        $volumes = $null
        $volumes = gwmi win32_volume -filter "DriveType = '3'" | ?{$_.driveletter}
        If($volumes | ?{$_.driveletter -eq $cddl})
        {
            $chdl = (compare-object $dls $volumes.driveletter | ?{$_.sideindicator -eq "<="} | select -First 1).inputobject
            $chvol = $volumes | ?{$_.driveletter -eq $cddl}
            $chvol.driveletter = $chdl
            $chvol.put()
        }
        $cdrom.driveletter = $cddl
        $cdrom.put()
    }

#**************************************************
# Finds any offline disks and goes through each
# drive that was online on the original VM by it's
# volume serial number to see if it's online and if
# it is and doesn't have it's drive letter, then
# does the same thing as what the CDROM went
# through.  If drive is not found by volume serial
# number, then look for offline disk that closely
# matches the drive's size and brings the disk
# online.
#**************************************************
    $gd = get-disk | ?{$_.OperationalStatus -eq "Offline"}
    $dri | %{
        $logdr = gwmi win32_logicaldisk -Filter "DriveType = '3'"
        $isplit = $_.split("|")
        $fvol = $null
        $fvol = $logdr | ?{$_.volumeserialnumber -eq $isplit[3]}
        If(!($fvol) -or $fvol.deviceid -ne $isplit[0])
        {
            If($fvol -and $fvol.deviceid -ne $isplit[0])
            {
                $fvolname = $null
                $fvolname = $logdr | ?{$_.deviceid -eq $isplit[0]}
                If($fvolname)
                {
                    $volumes = $null
                    $volumes = gwmi win32_volume -filter "DriveType = '3'" | ?{$_.driveletter}
                    $chdl = (compare-object $dls $volumes.driveletter | ?{$_.sideindicator -eq "<="} | select -First 1).inputobject
                    $changevol = gwmi win32_volume -filter "DriveType = '3'" | ?{$_.driveletter -eq $isplit[0]}
                    $changevol.driveletter = $chdl
                    $changevol.put()
                }
                $vol = gwmi win32_volume -filter "DriveType = '3'" | ?{$_.driveletter -eq $fvol.deviceid}
                If($vol)
                {
                    $vol.driveletter = $isplit[0]
                    $vol.put()
                }
            }
            ElseIf(!($fvol) -and $gd)
            {
                $fgd = $null
                $fgd = $gd | ?{$_."Total Size" -eq $isplit[2] -or $_."Total Size" -eq $isplit[2]-1}
                If($fgd -and ($fgd | measure-object).count -eq 1){$fgd | set-disk -IsOffline $false}
                Else{$prob += "Problem with Drive - $($ipslit[0])"}
            }
            Else{$prob += "Problem with Drive - $($ipslit[0])"}
        }
    }

#**************************************************
# If there's one NIC and it's enabled with DHCP,
# then add the IP info including DNS and ping the
# gateway.  Also run Windows Update cmds.
#**************************************************
    $fnet = $null
    $fnet = Get-NetAdapter | ?{$_.Status -eq "up"}
    If($fnet -and ($fnet | measure-object).count -eq 1)
    {
        If((gwmi Win32_NetworkAdapterConfiguration -Filter IPEnabled=TRUE).dhcpenabled -eq $true)
        {
            $fnet | New-NetIPAddress -IPAddress $priIP[0] -PrefixLength $priIP[1] -DefaultGateway $priIP[2] -SkipAsSource $false
            $fnet | Set-DnsClientServerAddress -ServerAddresses $dns
            If($addIP){$addIP | %{$fnet | New-NetIPAddress -IPAddress $_ -PrefixLength $priIP[1] -SkipAsSource $true}}
        }
        sleep -s 2
        $ping = ping $priIP[2] -n 3
        w32tm /resync /force
        If(!($ping | ?{$_ -match "Reply from"})){$prob += "Problem with network"}
        If($srv -match "TASK")
        {
            $pingo = ping 10.25.25.254 -n 3
            If(!($pingo | ?{$_ -match "Reply from"})){$prob += "Problem pinging Oracle gateway"}
            $fservice = $null
            $fservice = get-service -Name "T2*Scheduler*"
            If($fservice -and $fservice.status -ne "Running")
            {
                $fservice | Start-Service -Confirm:$false
                If((get-service -Name "T2*Scheduler*").Status -ne "Running"){$prob += "Failed to start T2 Flex Scheduler service"}
                Else{$prob += "Had to start T2 Flex Scheduler service"}
            }
            ElseIf(!($fservice)){$prob += "Could not find T2 Flex Scheduler service"}
        }

        . C:\Windows\system32\wuauclt.exe /resetauthorization
        If((gwmi win32_operatingsystem).caption -match '2012|2008'){. C:\Windows\system32\wuauclt.exe /detectnow}
        Else{(New-Object -ComObject Microsoft.Update.AutoUpdate).DetectNow()}
        . C:\Windows\system32\wuauclt.exe /reportnow
    }

#**************************************************
# Uses devcon.exe to remove the Hyper-V ghost NIC
#**************************************************
    . C:\IT\devcon.exe -r remove "@$($nicreg)"
    $checknic = . C:\IT\devcon.exe findall =net
    If($checknic -match "hyper"){$prob += "Problem removing ghost NIC"}
    
#**************************************************
# Turns on the Windows firewall if it was on
# originally.  Then send email with results
#**************************************************
    If($wfw -eq "On"){Set-NetFirewallProfile -Profile Domain -Enabled True}
    $sub = "V2V migration of $($srv.toupper())"
    $from = "V2V@t2systems.com"
    $body = "$($srv.toupper())`n`n"
    If(($prob | measure-object).count -gt 0){$prob | %{$body += "$($_)`n"}}
    Else{$body += "SUCCESS!  But, it's always good to check."}

    Send-MailMessage -To $to -From $from -Subject $sub -Body $body -SmtpServer t2prelay35.t2hosted.com

    Remove-Item "C:\IT\info_v2v.txt" -Force
    Remove-Item "C:\IT\devcon.exe" -Force
    Unregister-ScheduledTask -TaskName "V2V-Post" -Confirm:$false
    Remove-Item "C:\IT\v2v-post-config.ps1" -Force
}

