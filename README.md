## V2V pre and post conversion scripts

To be used before VMware vCenter Conversion Standalone tool.

Only the pre scripts should be ran on a Jump server.

*Below are descriptions and examples of the scripts*

---
** Steps of process **

1. Copy scripts and devcon.exe to Jump server
2. Remote into Jump server
3. Open PowerShell - Does not need to be administrator
4. (Optional) PS C:\Users\kevingardner>. C:\IT\v2v-pre-check.ps1 -hyperv **Hyper-V**
5. PS C:\Users\kevingardner>. C:\IT\v2v-pre-config.ps1 -vms **vm1,vm2,vm3**
6. Run VMware vCenter Converter Standalone and configure VMs for conversion
7. Check emails and\or VMs that were converted for status

---
** v2v-pre-check.ps1 **

This script can be ran any time before running the conversion tool.  It will gather info on all running Hyper-V Windows VMs by providing the Hyper-V server(s).

It will gather all info, such as OS, Drive and Network Info, etc..  It will then display the list where you will see what VMs will work with the migration scripts.

Error messages will be seen for any issues including trying to connect to Linux VMs.  However, they can be ignored as the results will show empty columns for VMs that have issues.

Paramerter: -hyperv

* Examples of running the script:
	* `PS C:\Users\kevingardner>. C:\IT\v2v-pre-check.ps1 -hyperv C:\IT\hyperv_server_list.txt`
	* `PS C:\Users\kevingardner>. C:\IT\v2v-pre-check.ps1 -hyperv T2PMSH106,T2PMSH107`
	* `PS C:\Users\kevingardner>. C:\IT\v2v-pre-check.ps1 -hyperv T2PMSH108`

Example of the output:
```
    VM            : FLX-P-FLXP111C
    OS            : Microsoft Windows Server 2012 R2 Datacenter
    Admin         : Good
    NICs          : 1
    PrimaryIPInfo : 10.91.91.32|24|10.91.91.254
    AdditionalIPs : 10.91.91.132
    Ping          : Success
    Drives        : C:|3|59|0A3394FF,D:|3|16|148E82D8
    CDROM         : None
    Firewall      : On
    Hyper-V       : T2PMSH108
```

---

** v2v-pre-config.ps1 **

This script needs to be ran before doing anything in the conversion tool as it will gather info for each VM, then copy files and create a scheduled task on the VMs.

If network is establish it will email the result for each VM to noc@t2systems.com.  It you want additional email recipients, you can add them to the -to parameter

Parameter: -vms -to (Optional)

* Examples of running the script:
	* `PS C:\Users\kevingardner>. C:\IT\v2v-pre-config -vms C:\IT\vm_list.txt`
	* `PS C:\Users\kevingardner>. C:\IT\v2v-pre-config.ps1 -vms FLX-P-FLX101A,FLX-P-FLX101B,FLX-P-FLX101C`
	* `PS C:\Users\kevingardner>. C:\IT\v2v-pre-config.ps1 -vms FLX-P-FLX101A -to kevin.gardner@t2systems.com`


---

** v2v-post-config.ps1 **

This script gets copied to each VM to run from a scheduled task that is triggered by VMware Tools service starting.  It will update drives, CDROM, IP, remove ghost NIC, turn Windows firewall on if it was before conversion, and then remove files and the scheduled task.

Do not run this script from the Jump server.  You should only run it manually from a VM that has been converted if the scheduled task failed or any other reason the script did not run.